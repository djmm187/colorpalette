/**
 * Basic API utility to fetch data and handle response.
 * NOTE: The class only supports `GET` requests
 *
 * @class API
 */
 var API = function(cfg) {
    var cfg = cfg || {};
    var httpRequest = new XMLHttpRequest();

    /**
     * Response handler for request
     * @name handleResponse
     * @private
     * @param {Object} callback optional callback object containing scope and method
     * @returns {Function|JSON} callback function or raw JSON response
     */
    var handleResponse = function(callback) {
        if (httpRequest.readyState === XMLHttpRequest.DONE) {

            // make sure we have actually got the data
            if (httpRequest.status === 200) {
                var response = JSON.parse(httpRequest.responseText);

                // pass response to callback if provided or just return the response
                return (callback) ? callback.fn.call(callback.scope, response) : response;

            } else {
                console.error(httpRequest.status, ' There was a problem with the request.');
            }
        }
    }

    /**
     * Public GET method
     * @name fetch
     * @param {String} url [Required] endpoint for request
     * @param {Object} callback optional callback for request handler
     */
    this.fetch = function(url, callback) {
        if (!httpRequest) {
            console.error('you need to use a modern browser!! NO XMLHTTP instance could be created. :(');
            return;
        }

        httpRequest.onreadystatechange = function() {
            handleResponse(callback);
        };

        httpRequest.open('GET', url);
        httpRequest.send();
    }

    // check to see if we need to automatically fetch data
    if (cfg.autoLoad) {

        // make sure we have an endpoint
        if (!cfg.url) {
            throw 'Please specify a data source before loading';
        }

        // call our get method and pass url and call back object
        this.fetch(
            cfg.url,
            cfg.callback
        );
    }

    return this;
};