/**
 * Basic DataStore module to manage a collection of models/objects
 *
 * @class DataStore
 */
var DataStore = function(cfg) {
    var cfg = cfg || {};

    this.__data = cfg.data || []; // our local data store
    this.__current = 0; // current pointer
    this.apiConfig = cfg.apiConfig || {};

    /**
     * Updates the model collection
     * @name setData
     * @returns {Array} collection of models
     */
    this.setData = function(data) {
        this.__data = data;

        return this.getData();
    };

    /**
     * Returns all data within the data store
     * @name getData
     * @returns {Array} collection of models
     */
    this.getData = function() {
        return this.__data;
    };

    /**
     * Returns specific model based on index
     * @name getAt
     * @param {Integer} model reference
     * @returns {Object} current model
     */
    this.getAt = function(index) {
        return this.__data[index]
    };

    /**
     * Updates the current model
     * @name setCurrent
     * @param {Integer} index in data store
     * @returns {Object} current model
     */
    this.setCurrent = function(pointer) {
        this.__current += pointer;

        return this.getCurrent();
    };

    /**
     * Returns the current model
     * @name getCurrent
     * @returns {Object} model
     */
    this.getCurrent = function() {
        return this.__data[this.__current];
    };

    /**
     * Returns whether or not this is the first model
     * @name isLast
     * @returns {Boolean}
     */
    this.isFirst = function() {
        return this.__current === 0;
    };

    /**
     * Returns whether or not this is the last model
     * @name isLast
     * @returns {Boolean}
     */
    this.isLast = function() {
        return this.__current === this.__data.length - 1;
    };

    /**
     * Api call back to set our response data in our datastore
     * @name onLoad
     * @param {Array} collection of models
     */
    this.onLoad = function(data) {
        // set our initial dataset
        this.setData(data);
    };

    // autoload results
    if (cfg.autoLoad) {
        this.apiConfig.callback = {
            'fn': this.onLoad,
            'scope': this
        };
        this.apiConfig.autoLoad = true;

        new API(this.apiConfig);
    }

    return this;
};