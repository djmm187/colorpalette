/**
 * Simple module to view color palettes
 *
 * @class ColorPaletteView
 */
var ColorPaletteView = function(cfg) {
    this.store = cfg.store || new DataStore(); // passed in store or create an empty store
    this.__container = document.querySelector(cfg.container || 'body'); // module target

    // update the view automatically, if needed
    if (cfg.autoLoad) {
        setTimeout(function() {
            this.init();
        }.bind(this), 1000);
    }
};

/**
 * Binds nav buttons and loads the initial palette model from our data store
 * @name init
 */
ColorPaletteView.prototype.init = function() {
    var btns = document.querySelectorAll('.triangle') || [];

    // bind our navigation buttons
    for (var b = 0; b < btns.length; b++) {
        btns[b].addEventListener('click', function(event) {
            this[event.target.dataset.nav]();
        }.bind(this));
    }

    this.showPalette();
};

/**
 * Triggers update in our data store and tells view to update with the latest
 * color palette
 * @name next
 */
ColorPaletteView.prototype.next = function() {
    if (this.store.isLast()) return;

    // update store and view
    this.store.setCurrent(1);
    this.showPalette();
};

/**
 * Triggers update in our data store and tells view to update with the previous
 * color palette
 * @name previous
 */
ColorPaletteView.prototype.previous = function(){
    if (this.store.isFirst()) return;

    // update store and view
    this.store.setCurrent(-1);
    this.showPalette();
};

/**
 * Grabs a current or specified palette model and uses the predefined dom template
 * to display the model properties (metadata)
 * @name showPalette
 */
ColorPaletteView.prototype.showPalette = function(selection) {
    var palette = selection || this.store.getCurrent(),
        metaContainer = this.__container,
        colors = palette.colors,
        title = palette.title,
        user = palette.userName,
        desc = palette.description,
        src = palette.url,
        hex = '';

    this.setBackground(colors);

    metaContainer.querySelector('#title').innerHTML = title;
    metaContainer.querySelector('#author').innerHTML = user;
    metaContainer.querySelector('#source').innerHTML = '<a href="' + src + '" target="_blank">'+ src + '</a>';
    metaContainer.querySelector('#hex').innerHTML = colors.join(', ');
};

/**
 * Sets the viewport background to the model selection
 * @name setBackground
 */
ColorPaletteView.prototype.setBackground = function(colors) {
    var colorParent = document.getElementById('colors'),
        palHeight = 100 / colors.length;

    colorParent.innerHTML = '';

    for (var c in colors) {
        var colorDiv = document.createElement('div');
        colorDiv.style.cssText = 'background-color: #'+ colors[c] + '; height: '+ palHeight + '%; width: 100%';
        colorParent.appendChild(colorDiv);
        hex += '#' + colors[c] + ' ';
    }
};

